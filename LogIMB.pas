unit LogIMB;

interface

uses
  SysUtils,
  Logger,
  IMB3Core,
  IMB3NativeClient;

const
  DefaultLogEventName = 'Log';
  DefaultUsePrefix = True;

type
  TIMBLogger = class(TLogBase)
  constructor Create(aLog: TLog; aConnection: TIMBConnection; const aLogEventName: string = DefaultLogEventName; aUsePrefix: Boolean=DefaultUsePrefix);
  private
    fLogEvent: TIMBEventEntry;
  public
    procedure WriteLn(const aLine: string; aLevel: Logger.TLogLevel); override;
  end;

function AddIMBLogger(aLog: TLog; aConnection: TIMBConnection; const aLogEventName: string = DefaultLogEventName): TIMBLogger;

implementation

function AddIMBLogger(aLog: TLog; aConnection: TIMBConnection; const aLogEventName: string): TIMBLogger;
begin
  Result := TIMBLogger.Create(aLog, aConnection, aLogEventName);
end;

{ TLogIMB }

constructor TIMBLogger.Create(aLog: TLog; aConnection: TIMBConnection; const aLogEventName: string; aUsePrefix: Boolean);
begin
  fLogEvent := aConnection.Publish(aLogEventName, aUsePrefix);
  inherited Create(aLog);
  SetLogDef(AllLogLevels, [llsNoPre]);
  SetLogDef([llRemark, llDump, llNormal], [llsNotActive]);
end;

procedure TIMBLogger.WriteLn(const aLine: string; aLevel: Logger.TLogLevel);
begin
  if Assigned(fLogEvent)
  then fLogEvent.LogWriteLn(fLogEvent.Connection.OwnerName + '(' + fLogEvent.Connection.OwnerID.ToString + '): ' + aLine, Integer(aLevel));
end;

end.
