unit LogIMB5;

interface

uses
  SysUtils,
  Logger,
  imb5;

const
  DefaultLogEventName = 'Log';
  DefaultUsePrefix = True;

type
  TIMBLogger = class(TLogBase)
  constructor Create(aConnection: TConnection; const aLogEventName: string = DefaultLogEventName; aUsePrefix: Boolean=DefaultUsePrefix);
  private
    fLogEvent: TEventEntry;
  public
    procedure WriteLn(const aLine: string; aLevel: Logger.TLogLevel); override;
  end;

function AddIMBLogger(aConnection: TConnection; const aLogEventName: string = DefaultLogEventName): TIMBLogger;

implementation

function AddIMBLogger(aConnection: TConnection; const aLogEventName: string): TIMBLogger;
begin
  Result := TIMBLogger.Create(aConnection, aLogEventName);
end;

{ TIMBLogger }

constructor TIMBLogger.Create(aConnection: TConnection; const aLogEventName: string; aUsePrefix: Boolean);
begin
  fLogEvent := aConnection.eventEntry(aLogEventName, aUsePrefix).Publish();
  inherited Create(Log);
  SetLogDef(AllLogLevels, [llsNoPre]);
end;

procedure TIMBLogger.WriteLn(const aLine: string; aLevel: Logger.TLogLevel);
begin
  if Assigned(fLogEvent)
  then fLogEvent.signalLogEntry(fLogEvent.Connection.ModelName + '(' + fLogEvent.Connection.ModelID.ToString + '): ' + aLine, Integer(aLevel));
end;

end.
